<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 14:47
 */

namespace App\utils;


use App\entity\User;
use App\repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Security
 * @package App\utils
 */
class Security
{
    /**
     * session for is logged?
     */
    private const SESSION_LOGGED = 'is_logged';
    /**
     * session for logged user id
     */
    private const SESSION_ID = 'user_id';
    /**
     * pepper for pass hashing
     */
    private const PEPPER = 'jrN4jsNE2L0_E3';

    /**
     * Log user in
     *
     * @param User $user
     * @param Request $request
     */
    public static function login(User $user, Request $request)
    {
        $request->getSession()->set(self::SESSION_LOGGED, true);
        $request->getSession()->set(self::SESSION_ID, $user->getId());
    }

    /**
     * Log user out
     *
     * @param Request $request
     */
    public static function logout(Request $request)
    {
        $request->getSession()->set(self::SESSION_LOGGED, false);
        $request->getSession()->remove(self::SESSION_ID);
    }

    /**
     * Returns logged user
     *
     * @param Request $request
     * @return User|null
     */
    public static function getLogged(Request $request)
    {
        if ($request->getSession()->has(self::SESSION_LOGGED) && $request->getSession()->get(self::SESSION_LOGGED) === true) {
            $repo = new UserRepository();
            return $repo->getUserById($request->getSession()->get(self::SESSION_ID));
        }

        return null;
    }

    /**
     * Check if user is logged
     *
     * @param Request $request
     * @return bool
     */
    public static function isLogged(Request $request)
    {
        return self::getLogged($request) != null;
    }

    /**
     * hash password
     *
     * @param User $user
     * @param $plainPassword
     * @return User
     */
    public static function hasPassword(User $user, $plainPassword): User
    {
        $salt = uniqid(mt_rand(), true);
        $hash = hash('sha512', $salt . $plainPassword . self::PEPPER);

        $user->setPass($hash);
        $user->setSalt($salt);

        return $user;
    }

    /**
     * Get user by username and plain password
     *
     * @param $userName
     * @param $plainPassword
     * @return User|null
     */
    public static function checkPassword($userName, $plainPassword)
    {
        $repo = new UserRepository();
        $user = $repo->getUserByUserName($userName);

        if ($user && $user->getActive() == 1) {
            $hash = hash('sha512', $user->getSalt() . $plainPassword . self::PEPPER);

            if ($hash === $user->getPass()) {
                return $user;
            }
        }

        return null;
    }

    /**
     * Change password
     *
     * @param User $user
     * @param $plainPassword
     */
    public static function changePassword(User $user, $plainPassword)
    {
        $salt = uniqid(mt_rand(), true);
        $hash = hash('sha512', $salt . $plainPassword . self::PEPPER);

        $user->setPass($hash);
        $user->setSalt($salt);
    }

    /**
     * Check if user has access to route for roles
     *
     * @param $user User
     * @param array $roles
     * @return bool
     */
    public static function hasAccess($user, array $roles): bool
    {
        if (in_array('ROLE_USER', $roles)) {
            return true;
        }

        if (!$user) {
            return false;
        }

        if (in_array('ROLE_ADMIN', $roles) && $user->getAdmin() == 1) {
            return true;
        }

        if (in_array('ROLE_AUTHOR', $roles) && $user->getAuthor() == 1) {
            return true;
        }

        if (in_array('ROLE_REVIEWER', $roles) && $user->getReviewer() == 1) {
            return true;
        }

        return false;
    }
}