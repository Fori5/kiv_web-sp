<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 08.12.2018
 * Time: 21:17
 */

namespace App\utils;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class BaseTwigExtension
 * @package App\utils
 */
class BaseTwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var string root dir
     */
    private $root;
    /**
     * @var Request current request
     */
    private $request;
    /**
     * @var \App\entity\User|null current user
     */
    private $user;
    /**
     * @var UrlGenerator urlGenerator
     */
    private $urlGenerator;

    /**
     * BaseTwigExtension constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $config = include(__DIR__ . '/../config/config.php');
        $routes = include(__DIR__ . '/../config/routes.php');

        $this->root = $config['public_dir'];
        $this->request = $request;
        $this->user = Security::getLogged($request);

        $routeCollection = new RouteCollection();
        foreach ($routes as $name => $r) {
            $routeCollection->add($name, $r);
        }

        $context = new RequestContext();
        $context->fromRequest($request);

        $this->urlGenerator = new UrlGenerator($routeCollection, $context);
    }

    /**
     * Set global variables available in every template
     *
     * @return array
     */
    public function getGlobals()
    {
        /** @var Session $session */
        $session = $this->request->getSession();

        return array(
            'res' => $this->root,
            'res_css' => $this->root . '/css',
            'res_js' => $this->root . '/js',
            'res_img' => $this->root . '/img',
            'res_upload' => $this->root . '/upload',

            'app' => array(
                'user' => $this->user,
                'flashes' => $session->getFlashBag()->all()
            )
        );
    }

    /**
     * Create functions available in every template
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_Function('path', function ($route, $params = array()) {
                return $this->generateUrl($route, $params);
            }),
            new \Twig_Function('strip_tags', function ($content) {
                return strip_tags(trim(html_entity_decode($content, ENT_QUOTES, 'UTF-8'), "\xc2\xa0"));
            })
        );
    }

    /**
     * Generate url by route name and params
     *
     * @param $route
     * @param $params
     * @return string
     */
    private function generateUrl($route, $params)
    {
        return $this->urlGenerator->generate($route, $params);
    }


}