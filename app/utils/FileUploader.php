<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 22:41
 */

namespace App\utils;


use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileUploader
 * @package App\utils
 */
class FileUploader
{
    /**
     * Uploads file
     *
     * @param UploadedFile $file
     * @param $path
     * @param array $types
     * @return bool|string
     */
    public static function upload(UploadedFile $file, $path, $types = array())
    {
        if (in_array($file->getMimeType(), $types) === false) {
            return false;
        }

        if ($file->getSize() > 2097152) {
            return false;
        }


        $name = 'post' . date('m-d-Y_hia') . '.' . $file->getClientOriginalExtension();

        if (!move_uploaded_file($file->getRealPath(), $path . $name)) {
            return false;
        }

        return $name;
    }
}