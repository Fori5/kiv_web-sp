<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 12.12.2018
 * Time: 13:17
 */

namespace App\repository;


use App\entity\Notification;
use App\entity\User;

/**
 * Class NotificationRepository
 * @package App\repository
 */
class NotificationRepository extends BaseRepository
{
    /**
     * Insert new notification
     *
     * @param Notification $notification
     */
    public function addNotification(Notification $notification)
    {
        $this->db->insert('notifications', $this->asArray($notification));
    }

    /**
     * Returns all notification by user
     *
     * @param User $user
     * @return array
     */
    public function getNotifications(User $user)
    {
        $res = $this->db->queryAllRows('SELECT * FROM notifications WHERE id_user = ? AND is_read = 0 ORDER BY created DESC',
            array($user->getId()));

        $notifications = array();
        foreach ($res as $row) {
            $notifications[] = $this->createNotification($row);
        }

        $this->db->update('notifications', array('is_read' => 1), 'WHERE id_user = ?', array($user->getId()));

        return $notifications;
    }

    /**
     * Create notification from array
     *
     * @param $prop
     * @return Notification
     */
    private function createNotification($prop): Notification
    {
        $notification = new Notification();
        $notification->setId($prop['id']);
        $notification->setUserId($prop['id_user']);
        $notification->setTitle($prop['title']);
        $notification->setMessage($prop['message']);
        $notification->setRead($prop['is_read']);
        $notification->setCreated(new \DateTime($prop['created']));
        return $notification;
    }

    /**
     * Convert notification to array
     *
     * @param Notification $notification
     * @return array
     */
    private function asArray(Notification $notification): array
    {
        return array(
            'id_user' => $notification->getUserId(),
            'title' => $notification->getTitle(),
            'message' => $notification->getMessage(),
            'is_read' => $notification->getRead(),
            'created' => $notification->getCreated()->format("Y-m-d H:i:s")
        );
    }
}