<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 11.12.2018
 * Time: 12:06
 */

namespace App\repository;


use App\entity\Post;
use App\entity\Rating;
use App\entity\User;

/**
 * Class RatingsRepository
 * @package App\repository
 */
class RatingsRepository extends BaseRepository
{
    /**
     * rating type originality
     */
    public const RATING_ORIGINALITY = 1;
    /**
     * rating type topic
     */
    public const RATING_TOPIC = 2;
    /**
     * rating type technique quality
     */
    public const RATING_TECHNIQUE_QUALITY = 3;
    /**
     * rating type language quality
     */
    public const RATING_LANGUAGE_QUALITY = 4;

    /**
     * Returns all available ratings for rating type
     *
     * @param int $ratingType
     * @return array|null
     */
    public function getAvailableRatings(int $ratingType)
    {
        switch ($ratingType) {
            case self::RATING_ORIGINALITY:
                $table = 'ratings_originality';
                break;
            case self::RATING_TOPIC:
                $table = 'ratings_topic';
                break;
            case self::RATING_TECHNIQUE_QUALITY:
                $table = 'ratings_technic';
                break;
            case self::RATING_LANGUAGE_QUALITY:
                $table = 'ratings_lang';
                break;
            default:
                return null;
        }

        $res = $this->db->queryAllRows('SELECT * FROM ' . $table);

        return $res;
    }

    /**
     * Return review by post id and reviewer
     *
     * @param $postId
     * @param User $reviewer
     * @return Post|null
     */
    public function getPostReview($postId, User $reviewer)
    {
        $res = $this->db->queryOneRow('SELECT * FROM rating RIGHT JOIN posts ON rating.id_post = posts.id WHERE rating.id_user = ? AND rating.id_post =?',
            array($reviewer->getId(), $postId));

        if ($res) {
            $post = $this->createPost($res);
            $post->addRating($this->createRating($res));
            return $post;
        }

        return null;
    }

    /**
     * Return unpublished posts by reviewer
     *
     * @param User $reviewer
     * @return Post[]
     */
    public function getPostsToReviewByReviewer(User $reviewer)
    {
        $res = $this->db->queryAllRows('SELECT * FROM rating RIGHT JOIN posts ON rating.id_post = posts.id WHERE rating.id_user = ? AND posts.state = 0
ORDER BY posts.last_edit DESC;', array($reviewer->getId()));
        return $this->createPostsArray($res);
    }

    /**
     * Return published posts by reviewer
     *
     * @param User $reviewer
     * @return Post[]|null
     */
    public function getPostsReviewedByReviewer(User $reviewer)
    {
        $res = $this->db->queryAllRows('SELECT *,(
	SELECT SUM((originality + topic + technic_quality + lang_quality) / 4) / COUNT(*) 
          FROM rating
         WHERE id_post = p.id) total_average,
         SUM((r.originality + r.topic + r.technic_quality + r.lang_quality)/4) / COUNT(*) my_average
FROM rating r RIGHT JOIN posts p ON r.id_post = p.id WHERE r.id_user = ? AND p.state = 1

ORDER BY p.last_edit DESC
', array($reviewer->getId()));

        if ($res && sizeof($res) > 0 && $res[0]['id_post'] != null) {
            return $this->createPostsArray($res);
        }

        return null;
    }

    /**
     * Return unpublished, unrejected posts with ratings
     *
     * @return Post[]
     */
    public function getPostsWithRatings()
    {
        $res = $this->db->queryAllRows('SELECT *,u.id as uid,posts.id as pid FROM rating RIGHT JOIN posts ON rating.id_post = posts.id LEFT JOIN users as u ON rating.id_user = u.id WHERE posts.state = 0
ORDER BY posts.last_edit DESC;');

        /** @var Post[] $posts */
        $posts = array();
        foreach ($res as $row) {
            if (!isset($posts[$row['pid']])) {
                $posts[$row['pid']] = $this->createPost($row);
            }

            if (isset($row['id_post'])) {
                $posts[$row['pid']]->addRating($this->createRating($row));
            }
        }

        return $posts;
    }

    /**
     * Delete review
     *
     * @param $postId
     * @param $reviewerId
     */
    public function deleteReview($postId, $reviewerId)
    {
        $this->db->delete('rating', 'WHERE id_post = ? AND id_user = ?', array($postId, $reviewerId));
    }

    /**
     * create posts array from db result
     *
     * @param $res
     * @return Post[]
     */
    private function createPostsArray($res)
    {
        /** @var Post[] $posts */
        $posts = array();
        foreach ($res as $row) {
            $posts[] = $this->createPost($row);
            $posts[sizeof($posts) - 1]->addRating($this->createRating($row));
        }

        return $posts;
    }

    /**
     * Create new review
     *
     * @param Rating $rating
     */
    public function createReview(Rating $rating)
    {
        $this->db->insert('rating', $this->asArray($rating));
    }

    /**
     * Updates review
     *
     * @param Rating $rating
     */
    public function updateReview(Rating $rating)
    {
        $this->db->update('rating', $this->asArray($rating), 'WHERE id_post = ? AND id_user = ?',
            array($rating->getPostId(), $rating->getUserId()));
    }

    /**
     * Converts rating to array
     *
     * @param Rating $rating
     * @return array
     */
    private function asArray(Rating $rating): array
    {
        return array(
            'id_post' => $rating->getPostId(),
            'id_user' => $rating->getUserId(),
            'originality' => $rating->getOriginality(),
            'topic' => $rating->getTopic(),
            'technic_quality' => $rating->getTechnic(),
            'lang_quality' => $rating->getLang(),
            'comment' => $rating->getComment(),
            'rated' => $rating->getRated() ? $rating->getRated()->format("Y-m-d H:i:s") : null
        );
    }

    /**
     * Create post from array
     *
     * @param array $prop
     * @return Post
     */
    private function createPost(array $prop): Post
    {
        $post = new Post();
        $post->setId(isset($prop['pid']) ? $prop['pid'] : $prop['id']);
        $post->setAuthorId($prop['id_author']);
        $post->setTitle($prop['title']);
        $post->setAuthors($prop['authors']);
        $post->setAbstract($prop['abstract']);
        $post->setState($prop['state']);
        $post->setFile($prop['file']);
        $post->setCreated(new \DateTime($prop['created']));
        $post->setEdit(new \DateTime($prop['last_edit']));
        $post->setApproved($prop['approved'] != null ? new \DateTime($prop['approved']) : null);
        return $post;
    }

    /**
     * Craete rating from array
     *
     * @param array $prop
     * @return Rating
     */
    private function createRating(array $prop): Rating
    {
        $rating = new Rating();
        $rating->setPostId($prop['id_post']);
        $rating->setUserId($prop['id_user']);
        $rating->setOriginality($prop['originality']);
        $rating->setTopic($prop['topic']);
        $rating->setTechnic($prop['technic_quality']);
        $rating->setLang($prop['lang_quality']);
        $rating->setComment($prop['comment']);
        $rating->setRated($prop['rated'] != null ? new \DateTime($prop['rated']) : null);

        if (isset($prop['total_average'])) {
            $rating->setTotalAverage($prop['total_average']);
        }
        if (isset($prop['my_average'])) {
            $rating->setMyAverage($prop['my_average']);
        }

        if (isset($prop['uid'])) {
            $rating->setUser($this->createUser($prop));
        }

        return $rating;
    }

    /**
     * Create user from array
     *
     * @param array $prop
     * @return User
     */
    private function createUser(array $prop): User
    {
        $user = new User();
        $user->setId($prop['uid']);
        $user->setUserName($prop['user_name']);
        $user->setPass($prop['pass']);
        $user->setSalt($prop['salt']);
        $user->setName($prop['name']);
        $user->setSurname($prop['surname']);
        $user->setEmail($prop['email']);
        $user->setNumber($prop['number']);
        $user->setOrganization($prop['organization']);
        $user->setAuthor($prop['author']);
        $user->setReviewer($prop['reviewer']);
        $user->setAdmin($prop['admin']);
        $user->setActive($prop['active']);
        $user->setRegistration(new \DateTime($prop['registration']));
        return $user;
    }
}