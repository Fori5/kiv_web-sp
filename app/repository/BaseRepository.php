<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 14:26
 */

namespace App\repository;


use App\model\Database;

/**
 * Each repository max extends this on
 *
 * Class BaseRepository
 * @package App\repository
 */
class BaseRepository
{
    /**
     * @var Database
     */
    protected $db;

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        $this->db = Database::getInstance();
    }
}