<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 10.12.2018
 * Time: 23:55
 */

namespace App\repository;


use App\entity\Comment;
use App\entity\User;

/**
 * Class CommentsRepository
 * @package App\repository
 */
class CommentsRepository extends BaseRepository
{
    /**
     * Returns comments by post Id
     *
     * @param $postId
     * @return array
     */
    public function getCommentsByPostId($postId)
    {
        $res = $this->db->queryAllRows('SELECT *,u.id as uid FROM comments INNER JOIN users as u ON comments.id_user = u.id WHERE comments.id_post = ?', array($postId));

        $comments = array();
        foreach ($res as $row) {
            $comments[] = $this->createComment($row);
        }

        return $comments;
    }

    /**
     * Save comment
     *
     * @param Comment $comment
     */
    public function saveComment(Comment $comment)
    {
        $this->db->insert('comments', $this->asArray($comment));
    }

    /**
     * convert comment to array
     *
     * @param Comment $comment
     * @return array
     */
    private function asArray(Comment $comment): array
    {
        return array(
            'id_post' => $comment->getPostId(),
            'id_user' => $comment->getUserId(),
            'message' => $comment->getMessage(),
            'created' => $comment->getCreated()->format("Y-m-d H:i:s")
        );
    }

    /**
     * Create comment from array
     *
     * @param array $prop
     * @return Comment
     */
    private function createComment(array $prop): Comment
    {
        $comment = new Comment();
        $comment->setId($prop['id']);
        $comment->setPostId($prop['id_post']);
        $comment->setUserId($prop['id_user']);
        $comment->setMessage($prop['message']);
        $comment->setCreated(new \DateTime($prop['created']));

        if (isset($prop['uid'])) {
            $comment->setAuthor($this->createUser($prop));
        }

        return $comment;
    }

    /**
     * Crate user from array
     *
     * @param array $prop
     * @return User
     */
    private function createUser(array $prop): User
    {
        $user = new User();
        $user->setId($prop['id']);
        $user->setUserName($prop['user_name']);
        $user->setName($prop['name']);
        $user->setSurname($prop['surname']);
        $user->setEmail($prop['email']);
        $user->setNumber($prop['number']);
        $user->setOrganization($prop['organization']);
        $user->setAuthor($prop['author']);
        $user->setReviewer($prop['reviewer']);
        $user->setAdmin($prop['admin']);
        $user->setActive($prop['active']);
        $user->setRegistration(new \DateTime($prop['registration']));
        return $user;
    }
}