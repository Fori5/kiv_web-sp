<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 14:32
 */

namespace App\repository;


use App\entity\User;
use App\utils\Security;

/**
 * Class UserRepository
 * @package App\repository
 */
class UserRepository extends BaseRepository
{
    /**
     * Returns user by id
     *
     * @param int $id
     * @return User|null
     */
    public function getUserById(int $id)
    {
        $res = $this->db->queryOneRow('SELECT * FROM users WHERE id = ? AND deleted = 0', array($id));

        if ($res) {
            return $this->createUser($res);
        }

        return null;
    }

    /**
     * Returns user by username
     *
     * @param string $userName
     * @return User|null
     */
    public function getUserByUserName(string $userName)
    {
        $res = $this->db->queryOneRow('SELECT * FROM users WHERE user_name = ? AND deleted = 0', array($userName));

        if ($res) {
            return $this->createUser($res);
        }

        return null;
    }

    /**
     * Check if exists user with username
     *
     * @param string $username
     * @return bool
     */
    public function existsUserWithUserName(string $username): bool
    {
        $res = $this->db->queryOneRow('SELECT * FROM users WHERE user_name = ?', array($username));

        return $res != null;
    }

    /**
     * Check if exists user with email
     *
     * @param string $email
     * @return bool
     */
    public function existsUserWithEmail(string $email): bool
    {
        $res = $this->db->queryOneRow('SELECT * FROM users WHERE email = ?', array($email));

        return $res != null;
    }

    /**
     * Create new user
     * And before inserting has password
     *
     * @param User $user
     * @param $plainPassword
     * @return bool
     */
    public function registerUser(User $user, $plainPassword): bool
    {
        $user = Security::hasPassword($user, $plainPassword);
        $res = $this->db->insert('users', $this->asArray($user));

        return $res == 1;
    }

    /**
     * Updates user
     *
     * @param User $user
     */
    public function updateUser(User $user)
    {
        $this->db->update('users', $this->asArray($user), 'WHERE id = ?', array($user->getId()));
    }

    /**
     * Delete user (mark user as deleted)
     *
     * @param User $user
     */
    public function deleteUser(User $user)
    {
        //$this->db->delete('users', 'WHERE id = ?', array($user->getId()));
        $this->db->update('users', array('deleted' => 1), 'WHERE id = ?', array($user->getId()));
    }

    /**
     * Return all undeleted users
     *
     * @return array
     */
    public function getUsers()
    {
        $res = $this->db->queryAllRows('SELECT * FROM users WHERE deleted = 0');

        $users = array();
        foreach ($res as $row) {
            $users[] = $this->createUser($row);
        }

        return $users;
    }

    /**
     * Return all active reviewers
     *
     * @return array
     */
    public function getReviewers()
    {
        $res = $this->db->queryAllRows('SELECT * FROM users WHERE reviewer = 1 AND active = 1 AND deleted = 0');

        $users = array();
        foreach ($res as $row) {
            $users[] = $this->createUser($row);
        }

        return $users;
    }

    /**
     * Convert user to array
     *
     * @param User $user
     * @return array
     */
    private function asArray(User $user): array
    {
        return array(
            'user_name' => $user->getUserName(),
            'pass' => $user->getPass(),
            'salt' => $user->getSalt(),
            'name' => $user->getName(),
            'surname' => $user->getSurname(),
            'email' => $user->getEmail(),
            'number' => $user->getNumber(),
            'organization' => $user->getOrganization(),
            'author' => $user->getAuthor(),
            'reviewer' => $user->getReviewer(),
            'admin' => $user->getAdmin(),
            'active' => $user->getActive(),
            'registration' => $user->getRegistration()->format("Y-m-d H:i:s")
        );
    }

    /**
     * Create user from array
     *
     * @param array $prop
     * @return User
     */
    private function createUser(array $prop): User
    {
        $user = new User();
        $user->setId($prop['id']);
        $user->setUserName($prop['user_name']);
        $user->setPass($prop['pass']);
        $user->setSalt($prop['salt']);
        $user->setName($prop['name']);
        $user->setSurname($prop['surname']);
        $user->setEmail($prop['email']);
        $user->setNumber($prop['number']);
        $user->setOrganization($prop['organization']);
        $user->setAuthor($prop['author']);
        $user->setReviewer($prop['reviewer']);
        $user->setAdmin($prop['admin']);
        $user->setActive($prop['active']);
        $user->setRegistration(new \DateTime($prop['registration']));
        return $user;
    }
}