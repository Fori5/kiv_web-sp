<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 22:23
 */

namespace App\repository;


use App\entity\Post;

/**
 * Class PostsRepository
 * @package App\repository
 */
class PostsRepository extends BaseRepository
{
    /**
     * Returns post by id
     *
     * @param $id
     * @return Post|null
     */
    public function getPostById($id)
    {
        $res = $this->db->queryOneRow('SELECT * FROM posts WHERE id = ?', array($id));

        if ($res) {
            return $this->createPost($res);
        }

        return null;
    }

    /**
     * Insert post
     *
     * @param Post $post
     * @return int post id
     */
    public function insertPost(Post $post): int
    {
        $this->db->insert('posts', $this->asArray($post));
        return $this->db->lastInsertId();
    }

    /**
     * Updates post
     *
     * @param Post $post
     */
    public function updatePost(Post $post)
    {
        $this->db->update('posts', $this->asArray($post), 'WHERE id = ?', array($post->getId()));
    }

    /**
     * Deletes post
     *
     * @param Post $post
     */
    public function deletePost(Post $post)
    {
        $this->db->delete('posts', 'WHERE id = ?', array($post->getId()));
    }

    /**
     * Returns posts by author id
     *
     * @param $id
     * @return array
     */
    public function getListByAuthor($id)
    {
        $res = $this->db->queryAllRows('SELECT *,(
	SELECT SUM((originality + topic + technic_quality + lang_quality) / 4) / COUNT(*) 
          FROM rating
         WHERE id_post = p.id) total_average
FROM posts p WHERE p.id_author = ?

ORDER BY p.last_edit DESC', array($id));

        $posts = array();
        foreach ($res as $row) {
            $posts[] = $this->createPost($row);
        }

        return $posts;
    }

    /**
     * Return unpublished posts which are reviewing
     *
     * @return array
     */
    public function getUnpublishedPosts()
    {
        $res = $this->db->queryAllRows('SELECT * FROM posts WHERE state = 0 ORDER BY last_edit DESC');

        $posts = array();
        foreach ($res as $row) {
            $posts[] = $this->createPost($row);
        }

        return $posts;
    }

    /**
     * Get new posts
     *
     * @param int $limit
     * @return array
     */
    public function getNewPosts(int $limit)
    {
        $res = $this->db->queryAllRows('SELECT * FROM posts WHERE state = 1 ORDER BY approved DESC LIMIT ?', array($limit));

        $posts = array();
        foreach ($res as $row) {
            $posts[] = $this->createPost($row);
        }

        return $posts;
    }

    /**
     * Returns published posts
     *
     * @return array
     */
    public function getPublishedPosts()
    {
        $res = $this->db->queryAllRows('SELECT * FROM posts WHERE state = 1 ORDER BY approved DESC, last_edit DESC');

        $posts = array();
        foreach ($res as $row) {
            $posts[] = $this->createPost($row);
        }

        return $posts;
    }

    /**
     * Returns published posts with average rating
     *
     * @return array
     */
    public function getPublishedPostsWithRating()
    {
        $res = $this->db->queryAllRows('SELECT *,(
	SELECT SUM((originality + topic + technic_quality + lang_quality) / 4) / COUNT(*) 
          FROM rating
         WHERE id_post = p.id) total_average
FROM posts p WHERE p.state = 1

ORDER BY p.last_edit DESC');

        $posts = array();
        foreach ($res as $row) {
            $posts[] = $this->createPost($row);
        }

        return $posts;
    }

    /**
     * Returns rejected posts with average rating
     *
     * @return array
     */
    public function getRejectedPosts()
    {
        $res = $this->db->queryAllRows('SELECT *,(
	SELECT SUM((originality + topic + technic_quality + lang_quality) / 4) / COUNT(*) 
          FROM rating
         WHERE id_post = p.id) total_average
FROM posts p WHERE p.state = -1

ORDER BY p.last_edit DESC');

        $posts = array();
        foreach ($res as $row) {
            $posts[] = $this->createPost($row);
        }

        return $posts;
    }

    /**
     * Convert post to array
     *
     * @param Post $post
     * @return array
     */
    private function asArray(Post $post): array
    {
        return array(
            'id_author' => $post->getAuthorId(),
            'title' => $post->getTitle(),
            'authors' => $post->getAuthors(),
            'abstract' => $post->getAbstract(),
            'state' => $post->getState(),
            'file' => $post->getFile(),
            'created' => $post->getCreated()->format("Y-m-d H:i:s"),
            'last_edit' => $post->getEdit()->format("Y-m-d H:i:s"),
            'approved' => $post->getApproved() == null ? null : $post->getApproved()->format("Y-m-d H:i:s")
        );
    }

    /**
     * Create post from array
     *
     * @param array $prop
     * @return Post
     */
    private function createPost(array $prop): Post
    {
        $post = new Post();
        $post->setId($prop['id']);
        $post->setAuthorId($prop['id_author']);
        $post->setTitle($prop['title']);
        $post->setAuthors($prop['authors']);
        $post->setAbstract($prop['abstract']);
        $post->setState($prop['state']);
        $post->setFile($prop['file']);
        $post->setCreated(new \DateTime($prop['created']));
        $post->setEdit(new \DateTime($prop['last_edit']));
        $post->setApproved($prop['approved'] != null ? new \DateTime($prop['approved']) : null);

        if (isset($prop['total_average'])) {
            $post->setTotalRating($prop['total_average']);
        }

        return $post;
    }
}