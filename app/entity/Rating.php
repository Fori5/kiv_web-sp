<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 15:26
 */

namespace App\entity;

/**
 * Entity class for Rating
 *
 * Class Rating
 * @package App\entity
 */
class Rating
{
    /**
     * @var int
     */
    private $postId;
    /**
     * @var int
     */
    private $userId;
    /**
     * @var int|null
     */
    private $originality;
    /**
     * @var int|null
     */
    private $topic;
    /**
     * @var int|null
     */
    private $technic;
    /**
     * @var int|null
     */
    private $lang;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var \DateTime|null
     */
    private $rated;

    /**
     * @var User|null
     */
    private $user;

    private $myAverage;
    private $totalAverage;

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->postId;
    }

    /**
     * @param int $postId
     */
    public function setPostId(int $postId)
    {
        $this->postId = $postId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int|null
     */
    public function getOriginality()
    {
        return $this->originality;
    }

    /**
     * @param int|null $originality
     */
    public function setOriginality($originality)
    {
        $this->originality = $originality;
    }

    /**
     * @return int|null
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param int|null $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return int|null
     */
    public function getTechnic()
    {
        return $this->technic;
    }

    /**
     * @param int|null $technic
     */
    public function setTechnic($technic)
    {
        $this->technic = $technic;
    }

    /**
     * @return int|null
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param int|null $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return null|string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param null|string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime|null
     */
    public function getRated()
    {
        return $this->rated;
    }

    /**
     * @param \DateTime|null $rated
     */
    public function setRated($rated)
    {
        $this->rated = $rated;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getMyAverage()
    {
        if ($this->myAverage == null) {
            $this->myAverage = (
                    $this->originality +
                    $this->topic +
                    $this->technic +
                    $this->lang
                ) / 4;
        }

        return $this->myAverage;
    }

    /**
     * @param mixed $myAverage
     */
    public function setMyAverage($myAverage)
    {
        $this->myAverage = $myAverage;
    }

    /**
     * @return mixed
     */
    public function getTotalAverage()
    {
        return $this->totalAverage;
    }

    /**
     * @param mixed $totalAverage
     */
    public function setTotalAverage($totalAverage)
    {
        $this->totalAverage = $totalAverage;
    }

}