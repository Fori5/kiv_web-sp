<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 15:24
 */

namespace App\entity;

/**
 * Entity class for Post
 *
 * Class Post
 * @package App\entity
 */
class Post
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $authorId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $authors;
    /**
     * @var string
     */
    private $abstract;
    /**
     * @var int
     */
    private $state;
    /**
     * @var string
     */
    private $file;
    /**
     * @var \DateTime
     */
    private $created;
    /**
     * @var \DateTime
     */
    private $edit;
    /**
     * @var \DateTime|null
     */
    private $approved;

    /**
     * @var array
     */
    private $ratings = array();

    /**
     * @var float|null
     */
    private $totalRating;

    /**
     * @var array|null
     */
    private $reviewers;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId(int $authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAuthors(): string
    {
        return $this->authors;
    }

    /**
     * @param string $authors
     */
    public function setAuthors(string $authors)
    {
        $this->authors = $authors;
    }

    /**
     * @return string
     */
    public function getAbstract(): string
    {
        return $this->abstract;
    }

    /**
     * @param string $abstract
     */
    public function setAbstract(string $abstract)
    {
        $this->abstract = $abstract;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile(string $file)
    {
        $this->file = $file;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getEdit(): \DateTime
    {
        return $this->edit;
    }

    /**
     * @param \DateTime $edit
     */
    public function setEdit(\DateTime $edit)
    {
        $this->edit = $edit;
    }

    /**
     * @return \DateTime|null
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * @param \DateTime|null $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return array|Rating[]
     */
    public function getRatings(): array
    {
        return $this->ratings;
    }

    /**
     * @param array $ratings
     */
    public function setRatings(array $ratings)
    {
        $this->ratings = $ratings;
    }

    /**
     * @param Rating $rating
     */
    public function addRating(Rating $rating)
    {
        $this->ratings[] = $rating;

        $this->getReviewers();
    }

    /**
     * @return array
     */
    public function getReviewers()
    {
        $reviewers = array();
        /** @var Rating[] $ratings */
        $ratings = $this->ratings;
        foreach ($ratings as $rating) {
            $reviewers[$rating->getUserId()] = $rating->getUser();
        }

        $this->reviewers = $reviewers;

        return $reviewers;
    }

    /**
     * @return float|null
     */
    public function getTotalRating()
    {
        return $this->totalRating;
    }

    /**
     * @param float|null $totalRating
     */
    public function setTotalRating($totalRating)
    {
        $this->totalRating = $totalRating;
    }

    /**
     * @return bool
     */
    public function canAccept(): bool
    {
        $count = 0;
        /** @var Rating $rating */
        foreach ($this->ratings as $rating) {
            if ($rating->getRated() != null) $count++;
        }

        return $count >= 3;
    }
}