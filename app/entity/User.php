<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 14:23
 */

namespace App\entity;

/**
 * Entity class for user
 *
 * Class User
 * @package App\entity
 */
class User
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $userName;
    /**
     * @var string
     */
    private $pass;
    /**
     * @var string
     */
    private $salt;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $surname;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string|null
     */
    private $number;
    /**
     * @var string|null
     */
    private $organization;
    /**
     * @var int
     */
    private $author;
    /**
     * @var int
     */
    private $reviewer;
    /**
     * @var int
     */
    private $admin;
    /**
     * @var int
     */
    private $active;
    /**
     * @var \DateTime
     */
    private $registration;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass(string $pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt(string $salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param null|string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return null|string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param null|string $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return int
     */
    public function getAuthor(): int
    {
        return $this->author;
    }

    /**
     * @param int $author
     */
    public function setAuthor(int $author)
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getReviewer(): int
    {
        return $this->reviewer;
    }

    /**
     * @param int $reviewer
     */
    public function setReviewer(int $reviewer)
    {
        $this->reviewer = $reviewer;
    }

    /**
     * @return int
     */
    public function getAdmin(): int
    {
        return $this->admin;
    }

    /**
     * @param int $admin
     */
    public function setAdmin(int $admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active)
    {
        $this->active = $active;
    }

    /**
     * @return \DateTime
     */
    public function getRegistration(): \DateTime
    {
        return $this->registration;
    }

    /**
     * @param \DateTime $registration
     */
    public function setRegistration(\DateTime $registration)
    {
        $this->registration = $registration;
    }
}