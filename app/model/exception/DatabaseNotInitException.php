<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 08.12.2018
 * Time: 23:35
 */

namespace App\model\exception;

/**
 * Class DatabaseNotInitException
 * @package App\model\exception
 */
class DatabaseNotInitException extends \Exception
{

}