<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 08.12.2018
 * Time: 23:24
 */

namespace App\model;

use App\model\exception\DatabaseNotInitException;

/**
 * Database helper
 *
 * Class Database
 * @package App\model
 */
class Database
{
    /** @var \PDO */
    private $conn;

    /** @var Database|null */
    private static $instance = null;

    /**
     * Database constructor.
     * @param array|null $settings
     * @throws \Exception
     */
    private function __construct(array $settings = null)
    {
        try {
            $pdo = new \PDO(
                'mysql:host=' . $settings['host'] . ';dbname=' . $settings['dbname'],
                $settings['user'],
                $settings['pass'],
                array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
            );
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            $this->conn = $pdo;
        } catch (\PDOException $e) {
            throw new \Exception('Db connection failed: ' . $e->getMessage());
        }
    }

    /**
     * Returns instance or create new if settings is available
     *
     * @param array|null $settings
     * @return Database
     * @throws DatabaseNotInitException
     */
    public static function getInstance(array $settings = null): self
    {
        if ($settings != null) {
            self::$instance = new self($settings);
        } else if (self::$instance == null) {
            throw new DatabaseNotInitException('First init database with settings');
        }

        return self::$instance;
    }

    /**
     * Check if db is initialized
     *
     * @return bool
     */
    public static function isInit(): bool
    {
        return self::$instance != null;
    }

    /**
     * Runs sql query
     *
     * @param string $query
     * @param array $params
     * @return \PDOStatement
     * @throws \Exception
     */
    public function query(string $query, array $params = array()): \PDOStatement
    {
        try {
            $res = $this->conn->prepare($query);
            $res->execute($params);

            return $res;
        } catch (\PDOException $e) {
            throw new \Exception('Query failed ' . $e->getMessage());
        }
    }

    /**
     * Query one row
     *
     * @param string $query
     * @param array $params
     * @return mixed
     */
    public function queryOneRow(string $query, array $params = array())
    {
        return $this->query($query, $params)->fetch();
    }

    /**
     * Query all rows
     *
     * @param string $query
     * @param array $params
     * @return array
     */
    public function queryAllRows(string $query, array $params = array())
    {
        return $this->query($query, $params)->fetchAll();
    }

    /**
     * Query alone value
     *
     * @param string $query
     * @param array $params
     * @return mixed
     */
    public function queryAlone(string $query, array $params = array())
    {
        $res = $this->queryOneRow($query, $params);
        return $res[0];
    }

    /**
     * Insert in db
     *
     * @param string $table table name
     * @param array $params params
     * @return int number of affected rows
     */
    public function insert(string $table, array $params = array()): int
    {
        /** @noinspection SqlNoDataSourceInspection */
        /** @noinspection SqlDialectInspection */
        return $this->query("INSERT INTO `$table` (`" .
            implode('`, `', array_keys($params)) .
            "`) VALUES (" . str_repeat('?,', sizeOf($params) - 1) . "?)",
            array_values($params))->rowCount();
    }

    /**
     * Delete from db
     *
     * @param string $table table name
     * @param string $condition condition
     * @param array $params params
     * @return int number of affected rows
     */
    public function delete(string $table, string $condition, array $params = array()): int
    {
        /** @noinspection SqlNoDataSourceInspection */
        return $this->query("DELETE FROM `$table` " . $condition, $params)->rowCount();
    }

    /**
     * Update in db
     *
     * @param string $table table name
     * @param array $values values
     * @param string $condition condition
     * @param array $params params
     * @return int number of affected rows
     */
    public function update(string $table, array $values, string $condition, array $params = array())
    {
        /** @noinspection SqlNoDataSourceInspection */
        /** @noinspection SqlDialectInspection */
        return $this->query("UPDATE `$table` SET `" .
            implode('` = ?, `', array_keys($values)) .
            "` = ? " . $condition,
            array_merge(array_values($values), $params))
            ->rowCount();
    }

    /**
     * Returns last inserted id
     *
     * @return int
     */
    public function lastInsertId(): int
    {
        return $this->conn->lastInsertId();
    }

    private function __clone()
    {
    }
}