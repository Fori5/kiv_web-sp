<?php
/**
 * Routes
 *
 * Created by PhpStorm.
 * User: Martin
 * Date: 08.12.2018
 * Time: 17:38
 */

use Symfony\Component\Routing\Route;

if (!defined('CONTROLLER_METHOD_DELIMITER')) define('CONTROLLER_METHOD_DELIMITER', '::');

return array(

    'home' => new Route(
        '/',
        array(
            '_controller' => 'App\controller\frontend\MainController::homePage'
        )
    ),

    /* --------- STATIC PAGES ROUTES -------------- */

    'about' => new Route(
        '/about',
        array(
            '_controller' => 'App\controller\frontend\MainController::aboutConf'
        )
    ),

    /* --------- LOGIN REGISTRATION ROUTES -------------- */

    'login' => new Route(
        '/login',
        array(
            '_controller' => 'App\controller\frontend\LoginController::login'
        )
    ),

    'ajax_login' => new Route(
        '/ajax_login',
        array(
            '_controller' => 'App\controller\frontend\LoginController::loginAjax'
        )
    ),

    'logout' => new Route(
        '/logout',
        array(
            '_controller' => 'App\controller\frontend\LoginController::logout'
        )
    ),

    'registration' => new Route(
        '/registration',
        array(
            '_controller' => 'App\controller\frontend\RegistrationController::registration'
        )
    ),

    /* --------- PUBLIC POSTS ROUTES -------------- */

    'posts' => new Route(
        '/posts',
        array(
            '_controller' => 'App\controller\frontend\PostsController::listPosts'
        )
    ),

    'posts_detail' => new Route(
        '/posts/{id}',
        array(
            '_controller' => 'App\controller\frontend\PostsController::postDetail'
        ),
        array(
            'id' => '\d+'
        )
    ),

    /* --------- AUTHOR ROUTES ---------------- */

    'my_posts' => new Route(
        '/my_posts',
        array(
            '_controller' => 'App\controller\backend\AuthorController::myPosts',
            '_roles' => array('ROLE_AUTHOR')
        )
    ),

    'new_post' => new Route(
        '/new_post',
        array(
            '_controller' => 'App\controller\backend\AuthorController::newPost',
            '_roles' => array('ROLE_AUTHOR')
        )
    ),

    'edit_post' => new Route(
        '/edit_post/{id}',
        array(
            '_controller' => 'App\controller\backend\AuthorController::editPost',
            '_roles' => array('ROLE_AUTHOR')
        ),
        array(
            'id' => '\d+'
        )
    ),

    'delete_post' => new Route(
        '/delete_post/{id}',
        array(
            '_controller' => 'App\controller\backend\AuthorController::deletePost',
            '_roles' => array('ROLE_AUTHOR')
        ),
        array(
            'id' => '\d+'
        )
    ),

    /* --------- REVIEWER ROUTES ---------------- */

    'to_review' => new Route(
        '/to_review',
        array(
            '_controller' => 'App\controller\backend\ReviewerController::toReview',
            '_roles' => array('ROLE_REVIEWER')
        )
    ),

    'review' => new Route(
        '/review/{id}',
        array(
            '_controller' => 'App\controller\backend\ReviewerController::review',
            '_roles' => array('ROLE_REVIEWER')
        ),
        array(
            'id' => '\d+'
        )
    ),

    /* --------- ADMIN ROUTES ---------------- */

    // admin users routes

    'users' => new Route(
        '/users',
        array(
            '_controller' => 'App\controller\backend\AdminUsersController::users',
            '_roles' => array('ROLE_ADMIN')
        )
    ),

    'edit_user' => new Route(
        '/users/{id}',
        array(
            '_controller' => 'App\controller\backend\AdminUsersController::editUser',
            '_roles' => array('ROLE_ADMIN')
        ),
        array(
            'id' => '\d+'
        )
    ),

    // admin posts routes

    'posts_admin' => new Route(
        '/posts_admin',
        array(
            '_controller' => 'App\controller\backend\AdminPostsController::posts',
            '_roles' => array('ROLE_ADMIN')
        )
    ),

    'delete_review' => new Route(
        '/delete_review/{post}/{reviewer}',
        array(
            '_controller' => 'App\controller\backend\AdminPostsController::deleteReview',
            '_roles' => array('ROLE_ADMIN')
        ),
        array(
            'post' => '\d+',
            'reviewer' => '\d+',
        )
    ),

    'review_again' => new Route(
        '/review_again/{post}',
        array(
            '_controller' => 'App\controller\backend\AdminPostsController::reviewAgain',
            '_roles' => array('ROLE_ADMIN')
        ),
        array(
            'post' => '\d+'
        )
    ),

    'accept_review' => new Route(
        '/accept_review/{post}',
        array(
            '_controller' => 'App\controller\backend\AdminPostsController::acceptReview',
            '_roles' => array('ROLE_ADMIN')
        ),
        array(
            'post' => '\d+'
        )
    ),

    'reject_review' => new Route(
        '/reject_review/{post}',
        array(
            '_controller' => 'App\controller\backend\AdminPostsController::rejectReview',
            '_roles' => array('ROLE_ADMIN')
        ),
        array(
            'post' => '\d+'
        )
    ),

    /* --------- OTHER ROUTES ---------------- */

    'settings' => new Route(
        '/settings',
        array(
            '_controller' => 'App\controller\backend\UserController::settings',
            '_roles' => array('ROLE_ADMIN', 'ROLE_AUTHOR', 'ROLE_REVIEWER')
        )
    ),
);