<?php
/**
 * Main config file
 *
 * Created by PhpStorm.
 * User: Martin
 * Date: 08.12.2018
 * Time: 21:22
 */

return array(

    'public_dir' => '/web/public',
    #'public_dir' => '/public',

    'database' => array(
        'host' => 'localhost',
        'dbname' => 'web',
        'user' => 'root',
        'pass' => ''
        //'host' => 'wm110.wedos.net',
        //'dbname' => 'd125900_websp',
        //'user' => 'w125900_websp',
        //'pass' => 'vQjaBdpk'
    ),

);