<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 13:27
 */

namespace app\controller\backend;

use App\controller\BaseController;
use App\utils\Security;

/**
 * Each secure controller need to extend this one
 * And every route which may be secure must have protected function
 *
 * Class BackEndController
 * @package app\controller\backend
 */
abstract class BackEndController extends BaseController
{
    /**
     * Catch eny protected function calling
     * Try if user has right roles
     * If yes - call function
     * Else - redirect to login
     *
     * @param $name
     * @param $arguments
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function __call($name, $arguments)
    {
        if (!$this->getUser() || $this->getUser()->getActive() == 0) {
            Security::logout($this->getRequest());
            return $this->redirectToLogin($arguments);
        }

        $asoc = end($arguments);
        if (isset($asoc['_roles'])) {
            $roles = $asoc['_roles'];
            if (!Security::hasAccess($this->getUser(), $roles)) {
                return $this->redirectToLogin($arguments);
            }
        }

        return $this->$name(...$arguments);
    }

    /**
     * Redirects to login
     *
     * @param $arguments
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToLogin($arguments)
    {
        return $this->redirect('login');
    }

}