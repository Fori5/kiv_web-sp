<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 21:56
 */

namespace App\controller\backend;

use App\entity\Post;
use App\repository\PostsRepository;
use App\utils\FileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AuthorController
 * @package App\controller\backend
 */
class AuthorController extends BackEndController
{
    /**
     * My posts route
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function myPosts()
    {
        $repo = new PostsRepository();
        $posts = $repo->getListByAuthor($this->getUser()->getId());

        return $this->render('backend/my_posts.html.twig', array('posts' => $posts));
    }

    /**
     * New post route
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function newPost(Request $request)
    {
        $params = array();

        if ($request->isMethod('POST')) {
            $title = $request->request->get('title');
            $authors = $request->request->get('authors');
            $abstract = $request->request->get('abstract');
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $params['title'] = $title;
            $params['authors'] = $authors;
            $params['abstract'] = $abstract;

            if ($title == null || $authors == null || $abstract == null || $file == null) {
                $this->flashBag()->add('danger', 'Vyplňte všechny údaje.');
            } else {
                $path = __DIR__ . '/../../../public/upload/';

                $fileName = FileUploader::upload($file, $path, array('application/pdf'));
                if ($fileName === false) {
                    $this->flashBag()->add('danger', 'Chyba při nahrávání souboru');
                } else {
                    $post = new Post();
                    $post->setTitle($title);
                    $post->setAuthors($authors);
                    $post->setAbstract($abstract);
                    $post->setFile($fileName);
                    $post->setCreated(new \DateTime());
                    $post->setState(0);
                    $post->setAuthorId($this->getUser()->getId());
                    $post->setEdit(new \DateTime());

                    $repo = new PostsRepository();
                    $id = $repo->insertPost($post);

                    $this->flashBag()->add('success', 'Příspěvek úspešně vytvoře, čeká na schválení');
                    return $this->redirect('my_posts');
                }
            }
        }

        return $this->render('backend/new_post.html.twig', $params);
    }

    /**
     * Edit post route
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function editPost($id, Request $request)
    {
        $repo = new PostsRepository();
        $post = $repo->getPostById($id);

        if (!$post || $post->getState() == 1 || $post->getAuthorId() != $this->getUser()->getId()) {
            return $this->showError(404);
        }

        if ($request->isMethod('POST')) {
            $title = $request->request->get('title');
            $authors = $request->request->get('authors');
            $abstract = $request->request->get('abstract');
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $path = __DIR__ . '/../../../public/upload/';

            $post->setTitle($title);
            $post->setAuthors($authors);
            $post->setAbstract($abstract);
            $post->setEdit(new \DateTime());


            if ($title == null || $authors == null || $abstract == null) {
                $this->flashBag()->add('danger', 'Vyplňte všechny údaje.');
            } else {
                if ($file != null) {
                    $fileName = FileUploader::upload($file, $path, array('application/pdf'));
                    $post->setFile($fileName);

                    if($fileName === false) {
                        $this->flashBag()->add('danger', 'Chyba při nahrávání souboru.');
                        return $this->redirect('edit_post', array('id' => $id));
                    }
                }

                $repo->updatePost($post);
                $this->flashBag()->add('success', 'Příspěvek úspešně upraven');
                return $this->redirect('edit_post', array('id' => $id));
            }
        }

        return $this->render('backend/edit_post.html.twig', array('post' => $post));
    }

    /**
     * Delete post
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function deletePost($id, Request $request)
    {
        $repo = new PostsRepository();
        $post = $repo->getPostById($id);

        if (!$post || $post->getState() == 1 || $post->getAuthorId() != $this->getUser()->getId()) {
            return $this->showError(404);
        }

        $repo->deletePost($post);

        $this->flashBag()->add('success', 'Příspěvek smazán');

        return $this->redirect('my_posts');
    }
}