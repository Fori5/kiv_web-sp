<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 21:56
 */

namespace App\controller\backend;


use App\repository\UserRepository;
use App\utils\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package App\controller\backend
 */
class UserController extends BackEndController
{
    /**
     * Settings route
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function settings(Request $request)
    {
        if ($request->isMethod('POST')) {
            if ($request->request->has('save')) {
                return $this->saveData($request);
            } else if ($request->request->has('change_pass')) {
                return $this->changePassword($request);
            }
        }

        return $this->render('backend/settings.html.twig');
    }

    /**
     * Save changed user data
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function saveData(Request $request)
    {
        $phone = $request->request->get('phone');
        $organ = $request->request->get('organ');

        $user = $this->getUser();
        $user->setNumber($phone);
        $user->setOrganization($organ);

        $repo = new UserRepository();
        $repo->updateUser($user);

        $this->flashBag()->add('success', 'Údaje uloženy');

        return $this->redirect('settings');
    }

    /**
     * Change password
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function changePassword(Request $request)
    {
        $pass = $request->request->get('pass');
        $pass2 = $request->request->get('pass2');

        if ($pass != null && $pass == $pass2) {
            Security::changePassword($this->getUser(), $pass);
            $repo = new UserRepository();
            $repo->updateUser($this->getUser());
            $this->flashBag()->add('success', 'Heslo uloženo');
        } else {
            $this->flashBag()->add('danger', 'Hesla se neshodují');
        }

        return $this->redirect('settings');
    }
}