<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 12.12.2018
 * Time: 12:51
 */

namespace App\controller\backend;

use App\entity\User;
use App\repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminUsersController
 * @package App\controller\backend
 */
class AdminUsersController extends BackEndController
{
    /**
     * Admin list of users route
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function users()
    {
        $repo = new UserRepository();
        $users = $repo->getUsers();

        return $this->render('backend/users.html.twig', array('users' => $users));
    }

    /**
     * Edit user route
     *
     * @param int $id userId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function editUser($id, Request $request)
    {
        $repo = new UserRepository();
        $user = $repo->getUserById($id);

        if (!$user) {
            return $this->showError(404);
        }

        if ($request->isMethod('POST')) {
            if ($request->request->has('save')) {
                return $this->updateUser($user, $repo, $request);
            } else if ($request->request->has('block')) {
                return $this->blockUser($user, $repo, $request);
            } else if ($request->request->has('delete')) {
                return $this->deleteUser($user, $repo, $request);
            }
        }

        return $this->render('backend/edit_user.html.twig', array('user' => $user));
    }

    /**
     * Update user role
     *
     * @param User $user
     * @param UserRepository $repo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function updateUser(User $user, UserRepository $repo, Request $request)
    {
        $user->setAuthor($request->request->has('author') ? 1 : 0);
        $user->setReviewer($request->request->has('reviewer') ? 1 : 0);
        $user->setAdmin($request->request->has('admin') ? 1 : 0);

        $repo->updateUser($user);

        $this->flashBag()->add('success', 'Údaje uloženy');

        return $this->redirect('edit_user', array('id' => $user->getId()));
    }

    /**
     * Block/Unblock user
     *
     * @param User $user
     * @param UserRepository $repo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function blockUser(User $user, UserRepository $repo, Request $request)
    {
        $user->setActive($user->getActive() == 1 ? 0 : 1);
        $repo->updateUser($user);

        $this->flashBag()->add('success', $user->getActive() == 1 ? 'Uživatel doblokován' : 'Uživatel zablokován');

        return $this->redirect('edit_user', array('id' => $user->getId()));
    }

    /**
     * Delete user
     *
     * @param User $user
     * @param UserRepository $repo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function deleteUser(User $user, UserRepository $repo, Request $request)
    {
        $repo->deleteUser($user);

        return $this->redirect('users');
    }
}