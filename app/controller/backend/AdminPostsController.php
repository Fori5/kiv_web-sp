<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 13:29
 */

namespace App\controller\backend;


use App\entity\Rating;
use App\repository\PostsRepository;
use App\repository\RatingsRepository;
use App\repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminPostsController
 * @package App\controller\backend
 */
class AdminPostsController extends BackEndController
{
    /**
     * Route for managing all posts by admin
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function posts(Request $request)
    {
        $postsRepo = new PostsRepository();
        $usersRepo = new UserRepository();
        $ratingRepo = new RatingsRepository();

        $published = $postsRepo->getPublishedPostsWithRating();
        $unPublished = $ratingRepo->getPostsWithRatings();
        $rejected = $postsRepo->getRejectedPosts();

        $reviewers = $usersRepo->getReviewers();

        if ($request->isMethod('POST')) {
            if ($request->request->has('add_review')) {
                $reviewerId = $request->request->get('reviewer');
                $postId = $request->request->get('post_id');

                $rating = new Rating();
                $rating->setPostId($postId);
                $rating->setUserId($reviewerId);

                $ratingRepo->createReview($rating);

                $this->crateNotification($reviewerId, '<b>Nový příspěvek k posouzení</b>', 'Máte nový příspěvek k posouzení');

                return $this->redirect('posts_admin');
            }
        }

        return $this->render('backend/admin_posts.html.twig', array(
            'published' => $published,
            'unpublished' => $unPublished,
            'rejected' => $rejected,
            'reviewers' => $reviewers
        ));
    }

    /**
     * Delete review
     *
     * @param int $post postId
     * @param int $reviewer userId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function deleteReview($post, $reviewer, Request $request)
    {
        $repo = new RatingsRepository();
        $repo->deleteReview($post, $reviewer);

        $this->flashBag()->add('success', 'Hodnocení odstraněno');

        return $this->redirect('posts_admin');
    }

    /**
     * Opens post for reviewing
     *
     * @param $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function reviewAgain($post)
    {
        $repo = new PostsRepository();
        $post = $repo->getPostById($post);

        if ($post) {
            $post->setState(0);
            $repo->updatePost($post);
            $this->flashBag()->add('success', 'Příspěvek otevřen k posuzování.');
        }

        return $this->redirect('posts_admin');
    }

    /**
     * Accept review and publish post
     *
     * @param $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function acceptReview($post)
    {
        $repo = new PostsRepository();
        $post = $repo->getPostById($post);

        if ($post) {
            $post->setState(1);
            $post->setApproved(new \DateTime());
            $repo->updatePost($post);
            $this->flashBag()->add('success', 'Příspěvek přijat a publikován.');

            $this->crateNotification($post->getAuthorId(), '<b>Příspěvek publikován</b>', 'Váš příspvěvek ' . htmlspecialchars($post->getTitle()) . ' byl publikován');
        }

        return $this->redirect('posts_admin');
    }

    /**
     * Reject review
     *
     * @param $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function rejectReview($post)
    {
        $repo = new PostsRepository();
        $post = $repo->getPostById($post);

        if ($post) {
            $post->setState(-1);
            $repo->updatePost($post);
            $this->flashBag()->add('success', 'Příspěvek odmítnut.');
            $this->crateNotification($post->getAuthorId(), '<b>Příspěvek zamítnut</b>', 'Váš příspvěvek ' . htmlspecialchars($post->getTitle()) . ' byl zamítnut');
        }

        return $this->redirect('posts_admin');
    }
}