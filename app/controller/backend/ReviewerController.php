<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 21:56
 */

namespace App\controller\backend;


use App\entity\Rating;
use App\repository\RatingsRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ReviewerController
 * @package App\controller\backend
 */
class ReviewerController extends BackEndController
{
    /**
     * List of posts to review
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function toReview()
    {
        $repo = new RatingsRepository();
        $toReview = $repo->getPostsToReviewByReviewer($this->getUser());
        $reviewed = $repo->getPostsReviewedByReviewer($this->getUser());

        return $this->render('backend/to_review.html.twig', array(
            'to_review' => $toReview,
            'reviewed' => $reviewed
        ));
    }

    /**
     * Review detail route
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function review($id, Request $request)
    {
        $repo = new RatingsRepository();
        $postReview = $repo->getPostReview($id, $this->getUser());

        if ($postReview->getState() != 0 || $postReview->getRatings()[0]->getUserId() != $this->getUser()->getId()) {
            return $this->showError(404);
        }

        $ratingTypes = $this->getRatingTypes($repo);

        if ($request->isMethod('POST')) {
            $originality = $request->request->get('originality');
            $topic = $request->request->get('topic');
            $technic = $request->request->get('technic');
            $lang = $request->request->get('lang');

            if ($originality != null && $topic != null && $technic != null && $lang != null) {
                $message = $request->request->get('message');
                /** @var Rating $rating */
                $rating = $postReview->getRatings()[0];

                $rating->setOriginality($originality);
                $rating->setTopic($topic);
                $rating->setTechnic($technic);
                $rating->setLang($lang);
                $rating->setComment($message);
                $rating->setRated(new \DateTime());

                $repo->updateReview($rating);

                $this->flashBag()->add('success', 'Posudek pro <i>' . $postReview->getTitle() . '</i> uložen.');
                return $this->redirect('to_review');
            } else {
                $this->flashBag()->add('danger', 'Vyplňte všechna povinná pole!');
                return $this->redirect('review', array('id' => $id));
            }
        }

        return $this->render('backend/review.html.twig', array(
            'post' => $postReview,
            'rating_types' => $ratingTypes
        ));
    }

    /**
     * Return rating types with all available values
     *
     * @param RatingsRepository $repo
     * @return array
     */
    private function getRatingTypes(RatingsRepository $repo)
    {
        return array(
            'originality' => array(
                'title' => 'Originalita',
                'values' => $repo->getAvailableRatings(RatingsRepository::RATING_ORIGINALITY)
            ),
            'topic' => array(
                'title' => 'Téma',
                'values' => $repo->getAvailableRatings(RatingsRepository::RATING_TOPIC)
            ),
            'technic' => array(
                'title' => 'Technická kvalita',
                'values' => $repo->getAvailableRatings(RatingsRepository::RATING_TECHNIQUE_QUALITY)
            ),
            'lang' => array(
                'title' => 'Jazyková kvalita',
                'values' => $repo->getAvailableRatings(RatingsRepository::RATING_LANGUAGE_QUALITY)
            )
        );
    }
}