<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 08.12.2018
 * Time: 17:15
 */

namespace App\controller;

use App\entity\Notification;
use App\entity\User;
use App\model\Database;
use App\repository\NotificationRepository;
use App\utils\BaseTwigExtension;
use App\utils\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

/**
 * Base controller, each controller must extend this
 *
 * Class BaseController
 * @package App\controller
 */
abstract class BaseController
{
    /** @var  Request current request */
    private $request;
    /** @var \Twig_Environment twig */
    private $twig;
    /** @var  User|null current logged user or null */
    private $user = null;

    /**
     * BaseController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $loader = new \Twig_Loader_Filesystem('../app/view');
        $this->twig = new \Twig_Environment($loader, array());
        $this->twig->addExtension(new BaseTwigExtension($request));
        $this->request = $request;

        $this->user = Security::getLogged($request);
    }

    /**
     * Renders twig template
     *
     * @param $view string twig template path
     * @param array $parameters params
     * @param Response|null $response response
     * @return Response
     */
    protected function render($view, array $parameters = array(), Response $response = null)
    {
        if ($response === null) {
            $response = new Response();
        }

        if ($this->user) {
            $repo = new NotificationRepository();
            $notifications = $repo->getNotifications($this->user);

            /** @var Notification $notification */
            foreach ($notifications as $notification) {
                $this->flashBag()->add('info', $notification->getTitle() . '<br>' . $notification->getMessage());
            }
        }

        $response->setContent($this->twig->render($view, $parameters));

        return $response;
    }

    /**
     * Provides database instance
     *
     * @return Database database
     */
    protected function db(): Database
    {
        return Database::getInstance();
    }

    /**
     * Returns current request
     *
     * @return Request request
     */
    protected function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * Returns current logged user or null
     *
     * @return User|null user
     */
    protected function getUser()
    {
        return $this->user;
    }

    /**
     * Provides flashBag for adding flash message
     *
     * @return \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface
     */
    public function flashBag()
    {
        /** @var Session $session */
        $session = $this->request->getSession();
        return $session->getFlashBag();
    }

    /**
     * Creates new notification
     *
     * @param int $userId user id
     * @param string $title title
     * @param string $message message
     */
    public function crateNotification($userId, $title, $message)
    {
        $notification = new Notification();
        $notification->setUserId($userId);
        $notification->setTitle($title);
        $notification->setMessage($message);
        $notification->setRead(0);
        $notification->setCreated(new\DateTime());

        $repo = new NotificationRepository();
        $repo->addNotification($notification);
    }

    /**
     * Shows error page
     *
     * @param int $status status
     * @return Response
     */
    public function showError($status = 404)
    {
        switch ($status) {
            case 404:
                return $this->render('errors/error_404.html.twig', array('code' => $status), new Response('', $status));
        }

        return $this->render('errors/error_404.html.twig', array('code' => $status), new Response('', $status));
    }

    /**
     * Redirects to route
     *
     * @param string $route route name
     * @param array $params params
     * @param int $status redirect code
     * @return RedirectResponse
     */
    protected function redirect(string $route, array $params = array(), int $status = 302)
    {
        $routes = include(__DIR__ . '/../config/routes.php');
        $routeCollection = new RouteCollection();
        foreach ($routes as $name => $r) {
            $routeCollection->add($name, $r);
        }

        $context = new RequestContext();
        $context->fromRequest($this->request);

        $generator = new UrlGenerator($routeCollection, $context);
        $url = $generator->generate($route, $params);

        return new RedirectResponse($url, $status);
    }
}