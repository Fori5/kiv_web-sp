<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 20:06
 */

namespace App\controller\frontend;


use App\controller\BaseController;
use App\repository\PostsRepository;

/**
 * Class MainController
 * @package App\controller\frontend
 */
class MainController extends BaseController
{
    /**
     * Home page route
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homePage()
    {
        $repo = new PostsRepository();
        $posts = $repo->getNewPosts(3);

        return $this->render('frontend/index.html.twig', array('new_posts' => $posts));
    }

    /**
     * About conference route
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function aboutConf()
    {
        return $this->render('frontend/about.html.twig');
    }
}