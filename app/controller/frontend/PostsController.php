<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 20:07
 */

namespace App\controller\frontend;


use App\controller\BaseController;
use App\entity\Comment;
use App\entity\Post;
use App\repository\CommentsRepository;
use App\repository\PostsRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PostsController
 * @package App\controller\frontend
 */
class PostsController extends BaseController
{
    /**
     * List of all published posts route
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listPosts()
    {
        $repo = new PostsRepository();
        $posts = $repo->getPublishedPosts();

        return $this->render('frontend/posts_list.html.twig', array('posts' => $posts));
    }

    /**
     * Post detail route
     *
     * @param $id post id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postDetail($id, Request $request)
    {
        $repo = new PostsRepository();
        $post = $repo->getPostById($id);

        if (!$post) {
            return $this->showError(404);
        }

        $commentsRepo = new CommentsRepository();
        $comments = $commentsRepo->getCommentsByPostId($id);

        if ($request->isMethod('POST') && $this->getUser()) {
            return $this->addComment($post, $commentsRepo, $request);
        }

        return $this->render('frontend/posts_detail.html.twig', array('post' => $post, 'comments' => $comments));
    }

    /**
     * Add comment to post and redirect to commented post
     *
     * @param Post $post post
     * @param CommentsRepository $repo comments repository
     * @param Request $request current request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function addComment(Post $post, CommentsRepository $repo, Request $request)
    {
        if ($request->request->get('message') == null) {
            $this->flashBag()->add('danger', 'Zadejte text komentáře');
        } else {
            $comment = new Comment();
            $comment->setUserId($this->getUser()->getId());
            $comment->setPostId($post->getId());
            $comment->setMessage($request->request->get('message'));
            $comment->setCreated(new \DateTime());
            $repo->saveComment($comment);

            $this->flashBag()->add('success', 'Komentář přidán');
        }

        return $this->redirect('posts_detail', array('id' => $post->getId()));
    }
}