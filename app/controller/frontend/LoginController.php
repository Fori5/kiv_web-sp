<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 13:57
 */

namespace App\controller\frontend;

use App\controller\BaseController;
use App\utils\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoginController
 * @package App\controller\frontend
 */
class LoginController extends BaseController
{
    /**
     * Login route
     *
     * @param Request $request
     * @param bool $ajax
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function login(Request $request, $ajax = false)
    {
        if ($request->isMethod('POST')) {
            $name = $request->request->get('name');
            $pass = $request->request->get('pass');

            if ($name != null && $pass != null) {
                $user = Security::checkPassword($name, $pass);
                if ($user) {
                    Security::login($user, $request);
                    if ($ajax === true) {
                        return 'true';
                    }
                    return $this->redirect('home');
                } else {
                    if ($ajax === false)
                        $this->flashBag()->add('danger', 'Špatné jméno nebo heslo');
                }
            } else {
                if ($ajax === false)
                    $this->flashBag()->add('danger', 'Zadejte jméno a heslo');
            }
        }

        if ($ajax === true) {
            return 'false';
        }

        return $this->render('frontend/login.html.twig');
    }

    /**
     * Login via ajax
     * Return 'true' or 'false'
     *
     * @param Request $request
     * @return Response
     */
    public function loginAjax(Request $request)
    {
        return new Response($this->login($request, true));
    }

    /**
     * Logout route
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(Request $request)
    {
        Security::logout($request);
        return $this->redirect('login');
    }
}