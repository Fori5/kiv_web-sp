<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 09.12.2018
 * Time: 20:11
 */

namespace App\controller\frontend;

use App\controller\BaseController;
use App\entity\User;
use App\repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegistrationController
 * @package App\controller\frontend
 */
class RegistrationController extends BaseController
{
    /**
     * Registration route
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registration(Request $request)
    {
        $params = array();

        if ($request->isMethod('POST')) {
            $name = $request->request->get('name');
            $surname = $request->request->get('surname');
            $username = $request->request->get('user_name');
            $pass = $request->request->get('pass');
            $pass2 = $request->request->get('pass2');
            $email = $request->request->get('email');
            $phone = $request->request->get('phone');
            $organ = $request->request->get('organ');

            $params['name'] = $name;
            $params['surname'] = $surname;
            $params['user_name'] = $username;
            $params['email'] = $email;
            $params['phone'] = $phone;
            $params['organ'] = $organ;

            if ($name != null && $surname != null && $username != null && $pass != null && $pass2 != null && $email != null) {
                $repo = new UserRepository();
                $register = true;

                if ($repo->existsUserWithUserName($username)) {
                    $this->flashBag()->add('danger', 'Uživatelské jméno <i>' . htmlspecialchars($username) . '</i> již existuje');
                    $register = false;
                }
                if ($repo->existsUserWithEmail($email)) {
                    $this->flashBag()->add('danger', 'Účet s emailem <i>' . htmlspecialchars($email) . '</i> již existuje');
                    $register = false;
                }
                if ($pass != $pass2) {
                    $this->flashBag()->add('danger', 'Hesla se neshodují');
                    $register = false;
                }

                if ($register && $this->register($repo, $name, $surname, $username, $pass, $email, $phone, $organ)) {
                    $this->flashBag()->add('success', 'Registrace proběhla úspěšně, můžete se přihlásit');
                    return $this->redirect('login');
                }
            } else {
                $this->flashBag()->add('danger', 'Vyplňte všechna povinná pole!');
            }
        }

        return $this->render('frontend/registration.html.twig', $params);
    }

    /**
     * Creates new user in db
     *
     * @param UserRepository $repo repository
     * @param string $name user name
     * @param string $surname user surname
     * @param string $username username
     * @param string $pass password in plain text
     * @param string $email email
     * @param string $phone phone
     * @param string $organ organization
     * @return bool
     */
    private
    function register(UserRepository $repo, $name, $surname, $username, $pass, $email, $phone, $organ): bool
    {
        $user = new User();
        $user->setName($name);
        $user->setSurname($surname);
        $user->setUserName($username);
        $user->setEmail($email);
        $user->setNumber($phone);
        $user->setOrganization($organ);
        $user->setRegistration(new \DateTime());
        $user->setActive(1);
        $user->setAdmin(0);
        $user->setReviewer(0);
        $user->setAuthor(1);

        return $repo->registerUser($user, $pass);
    }
}