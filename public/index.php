<?php
/**
 * main point of application
 *
 * Created by PhpStorm.
 * User: Martin
 * Date: 08.12.2018
 * Time: 16:34
 */

require_once '../vendor/autoload.php';

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;

error_reporting(E_ERROR | E_PARSE);
$routes = include '../app/config/routes.php';

$routeCollection = new RouteCollection();
foreach ($routes as $name => $route) {
    $routeCollection->add($name, $route);
}

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

if (!$request->hasSession()) {
    $session = new \Symfony\Component\HttpFoundation\Session\Session();
    $request->setSession($session);
}

$context = new RequestContext();
$context->fromRequest($request);

$matcher = new \Symfony\Component\Routing\Matcher\UrlMatcher($routeCollection, $context);

/** @var array $parameters */
$parameters = null;
try {
    $parameters = $matcher->match($context->getPathInfo());
} catch (\Symfony\Component\Routing\Exception\ResourceNotFoundException $e) {
    $parameters = array(
        '_controller' => 'App\controller\frontend\MainController' . CONTROLLER_METHOD_DELIMITER . 'showError',
        'status' => 404
    );
}

$parts = explode(CONTROLLER_METHOD_DELIMITER, $parameters['_controller']);

if (!\App\model\Database::isInit()) {
    $config = include(__DIR__ . '/../app/config/config.php');
    $settings = $config['database'];
    \App\model\Database::getInstance($settings);
}

/** @var \App\controller\BaseController $controller */
$controller = new $parts[0]($request);
$method = $parts[1];

$realParams = array();
foreach ($parameters as $key => $value) {
    if (strpos($key, '_') !== 0) {
        $realParams[$key] = $value;
    }
}

/** @var \Symfony\Component\HttpFoundation\Response $response */
$params = array_values($realParams);
$params[] = $request;
$response = $controller->$method(...array_values($params), ...array_values(array($parameters)));

$response->send();