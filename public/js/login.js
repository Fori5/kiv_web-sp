$(document).ready(function () {

    $('#login-form').submit(function (e) {
        e.preventDefault();
        var name = $('#name').val();
        var pass = $('#pass').val();

        if (name === '') {
            showLoginMessage('Zadejte jméno!');
        } else if (pass === '') {
            showLoginMessage('Zadejte heslo!');
        } else {
            showLoginMessage('');
            $.ajax({
                type: 'post',
                url: $('#login_url').val(),
                data: $('form').serialize(),
                success: function (data) {
                    if(data === 'true') {
                        window.location.replace($('#login_redirect').val())
                    } else {
                        showLoginMessage('Špatné jméno nebo heslo')
                    }
                }
            });
        }
    })

});

function showLoginMessage(message) {
    $('#login-message').html(message)
}