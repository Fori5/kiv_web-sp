$(document).ready(function () {

    resizeAdminPostsTable();

    $(window).resize(resizeAdminPostsTable);
});

function resizeAdminPostsTable() {

    if($('.admin-posts-review-detail').css('display') === 'table-cell') {
        $('.admin-posts-review-form').attr('colspan', 7);
    } else {
        $('.admin-posts-review-form').attr('colspan', 3);
    }

}